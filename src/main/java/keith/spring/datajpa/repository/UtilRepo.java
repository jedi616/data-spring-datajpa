package keith.spring.datajpa.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class is currently only used in Unit Testing.
 * 
 * @author kjayme
 *
 */
@Repository
public class UtilRepo
{
    @PersistenceContext(unitName = "jpa-project")
    private EntityManager entityManager;

    /**
     * Used to reset data during Unit Testing.
     * 
     * @param entityClass
     * @throws Exception
     */
    @Transactional
    public void truncateTable(Class<?> entityClass) throws Exception
    {
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();

        Table tableAnnotation = entityClass.getAnnotation(Table.class);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("truncate table ");
        stringBuilder.append(tableAnnotation.name());
        entityManager.createNativeQuery(stringBuilder.toString()).executeUpdate();

        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
    }
}
