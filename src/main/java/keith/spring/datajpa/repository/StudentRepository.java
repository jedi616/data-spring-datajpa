package keith.spring.datajpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import keith.domain.jpa.entity.CourseM;
import keith.domain.jpa.entity.StudentM;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface StudentRepository extends JpaRepository<StudentM, Long>
{
    List<StudentM> findByCourseM(CourseM courseM);
}
