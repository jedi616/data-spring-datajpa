package keith.spring.datajpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import keith.domain.jpa.entity.CourseM;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface CourseRepository extends JpaRepository<CourseM, Long>
{
    List<CourseM> findByCourseName(String courseName);

    List<CourseM> findByCourseNameAndCourseIdNot(String courseName, Long courseId);
}
