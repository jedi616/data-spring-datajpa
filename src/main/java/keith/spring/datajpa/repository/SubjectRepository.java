package keith.spring.datajpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import keith.domain.jpa.entity.SubjectM;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface SubjectRepository extends JpaRepository<SubjectM, Long>
{
}
