package keith.spring.datajpa.mapper;

import org.springframework.stereotype.Component;

import keith.domain.dto.SubjectDto;
import keith.domain.jpa.entity.SubjectM;
import keith.spring.datajpa.mapper.generic.GenericMapperImpl;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Component
public class SubjectMapperImpl extends GenericMapperImpl<SubjectM, SubjectDto> implements SubjectMapper
{
}
