package keith.spring.datajpa.mapper;

import keith.domain.dto.SubjectDto;
import keith.domain.jpa.entity.SubjectM;
import keith.spring.datajpa.mapper.generic.GenericMapper;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface SubjectMapper extends GenericMapper<SubjectM, SubjectDto>
{
}
