package keith.spring.datajpa.mapper;

import keith.domain.dto.StudentDto;
import keith.domain.jpa.entity.StudentM;
import keith.spring.datajpa.mapper.generic.GenericMapper;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface StudentMapper extends GenericMapper<StudentM, StudentDto>
{
}
