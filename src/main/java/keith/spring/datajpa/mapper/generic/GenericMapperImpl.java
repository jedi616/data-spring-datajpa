package keith.spring.datajpa.mapper.generic;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.repository.JpaRepository;

import keith.domain.dto.SelectItem;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public abstract class GenericMapperImpl<E, D> implements GenericMapper<E, D>
{
    private Class<E> entityClass;
    private Class<D> dtoClass;

    @SuppressWarnings("unchecked")
    public GenericMapperImpl()
    {
        Type type = getClass().getGenericSuperclass();
        if (type instanceof ParameterizedType)
        {
            ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
            entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[0];
            dtoClass = (Class<D>) genericSuperclass.getActualTypeArguments()[1];
        }
        // Else is for Spring $$EnhancerBySpringCGLIB$$, instance is not used by
        // our code.
    }

    @Override
    public D mapToDto(E entity, String... ignoreProperties) throws Exception
    {
        if (entity != null)
        {
            D dto = dtoClass.newInstance();
            BeanUtils.copyProperties(entity, dto, ignoreProperties);
            return dto;
        }
        return null;
    }

    @Override
    public void mapToDto(D dto, E entity, String... ignoreProperties) throws Exception
    {
        if (entity != null)
        {
            BeanUtils.copyProperties(entity, dto, ignoreProperties);
        }
    }

    @Override
    public E mapToEntity(D dto) throws Exception
    {
        if (dto != null)
        {
            E entity = entityClass.newInstance();
            BeanUtils.copyProperties(dto, entity); // the ignoreProperties parameter is not included.
            return entity;
        }
        return null;
    }

    @Override
    public void mapToEntity(E entity, D dto) throws Exception
    {
        if (dto != null)
        {
            BeanUtils.copyProperties(dto, entity); // the ignoreProperties parameter is not included.
        }
    }

    @Override
    public List<D> mapToDtoList(List<E> entities) throws Exception
    {
        List<D> dtos = new ArrayList<D>();
        for (E entity : entities)
        {
            D dto = mapToDto(entity);
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public List<E> mapToEntityList(List<D> dtos) throws Exception
    {
        List<E> entities = new ArrayList<E>();
        for (D dto : dtos)
        {
            E entity = mapToEntity(dto);
            entities.add(entity);
        }
        return entities;
    }

    @Override
    public List<SelectItem> getSelectItems(List<E> entities) throws Exception
    {
        List<SelectItem> selectItems = new ArrayList<SelectItem>();
        for (E entity : entities)
        {
            SelectItem selectItem = new SelectItem();
            selectItem.setValue(getValueForSelectItem(entity));
            selectItem.setLabel(getLabelForSelectItem(entity));
            selectItems.add(selectItem);
        }
        return selectItems;
    }

    @Override
    public List<SelectItem> getSelectItems(List<E> entities, Comparator<SelectItem> comparator) throws Exception
    {
        List<SelectItem> selectItems = getSelectItems(entities);
        Collections.sort(selectItems, comparator);
        return selectItems;
    }

    protected long getValueForSelectItem(E entity) throws Exception
    {
        throw new Exception("'" + this.getClass().getName() + "' class should override 'getValueForSelectItem' method");
    }

    protected String getLabelForSelectItem(E entity) throws Exception
    {
        throw new Exception("'" + this.getClass().getName() + "' class should override 'getLabelForSelectItem' method");
    }

    protected <ME> ME getMemberEntity(ME memberEntity, long dtoMemberId, JpaRepository<ME, Long> repo) throws Exception
    {
        if (memberEntity != null)
        {
            long memberEntityId = getEntityId(memberEntity);
            if (memberEntityId == 0)
            {
                throw new Exception("Entity class '" + memberEntity.getClass().getName()
                        + "' should have a field annotated w/ @Id or Field w/ @Id should not be zero");
            }
            if (memberEntityId != dtoMemberId)
            {
                return repo.findOne(dtoMemberId);
            }
            return null;
        }
        else
        {
            return repo.findOne(dtoMemberId);
        }
    }

    private <ME> long getEntityId(ME memberEntity) throws Exception
    {
        for (Field field : memberEntity.getClass().getDeclaredFields())
        {
            if (field.isAnnotationPresent(Id.class))
            {
                field.setAccessible(true);
                Long id = (Long) field.get(memberEntity);
                return id.longValue();
            }
        }
        return 0;
    }
}
