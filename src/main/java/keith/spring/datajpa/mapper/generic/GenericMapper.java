package keith.spring.datajpa.mapper.generic;

import java.util.Comparator;
import java.util.List;

import keith.domain.dto.SelectItem;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface GenericMapper<E, D>
{
    D mapToDto(E entity, String... ignoreProperties) throws Exception;

    void mapToDto(D dto, E entity, String... ignoreProperties) throws Exception;

    E mapToEntity(D dto) throws Exception;

    void mapToEntity(E entity, D dto) throws Exception;

    List<D> mapToDtoList(List<E> entities) throws Exception;

    List<E> mapToEntityList(List<D> dtos) throws Exception;

    List<SelectItem> getSelectItems(List<E> entities) throws Exception;

    List<SelectItem> getSelectItems(List<E> entities, Comparator<SelectItem> comparator) throws Exception;
}
