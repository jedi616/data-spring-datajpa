package keith.spring.datajpa.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import keith.domain.dto.CourseDto;
import keith.domain.dto.StudentDto;
import keith.domain.jpa.entity.CourseM;
import keith.domain.jpa.entity.StudentM;
import keith.spring.datajpa.mapper.generic.GenericMapperImpl;
import keith.spring.datajpa.repository.CourseRepository;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Component
public class StudentMapperImpl extends GenericMapperImpl<StudentM, StudentDto> implements StudentMapper
{
    @Autowired
    private CourseRepository courseRepo;

    @Autowired
    private CourseMapper courseMapper;

    @Override
    public StudentDto mapToDto(StudentM entity, String... ignoreProperties) throws Exception
    {
        StudentDto dto = super.mapToDto(entity, (entity.getCourseM() == null) ? "course" : null);
        toDto(dto, entity);
        return dto;
    }

    @Override
    public void mapToDto(StudentDto dto, StudentM entity, String... ignoreProperties) throws Exception
    {
        super.mapToDto(dto, entity, (entity.getCourseM() == null) ? "course" : null);
        toDto(dto, entity);
    }

    private void toDto(StudentDto dto, StudentM entity) throws Exception
    {
        CourseDto course = courseMapper.mapToDto(entity.getCourseM());
        dto.setCourse(course);
    }

    @Override
    public StudentM mapToEntity(StudentDto dto) throws Exception
    {
        StudentM entity = super.mapToEntity(dto);
        toEntity(entity, dto);
        return entity;
    }

    @Override
    public void mapToEntity(StudentM entity, StudentDto dto) throws Exception
    {
        super.mapToEntity(entity, dto);
        toEntity(entity, dto);
    }

    private void toEntity(StudentM entity, StudentDto dto) throws Exception
    {
        if (dto.getCourse() != null && dto.getCourse().getCourseId() != 0)
        {
            CourseM courseM = getMemberEntity(entity.getCourseM(), dto.getCourse().getCourseId(), courseRepo);
            if (courseM != null)
            {
                entity.setCourseM(courseM);
            }
        }
    }
}
