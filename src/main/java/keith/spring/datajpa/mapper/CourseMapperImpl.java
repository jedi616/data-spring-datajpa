package keith.spring.datajpa.mapper;

import org.springframework.stereotype.Component;

import keith.domain.dto.CourseDto;
import keith.domain.jpa.entity.CourseM;
import keith.spring.datajpa.mapper.generic.GenericMapperImpl;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Component
public class CourseMapperImpl extends GenericMapperImpl<CourseM, CourseDto> implements CourseMapper
{
    @Override
    protected long getValueForSelectItem(CourseM entity) throws Exception
    {
        return entity.getCourseId();
    }

    @Override
    protected String getLabelForSelectItem(CourseM entity) throws Exception
    {
        return entity.getCourseName();
    }
}
