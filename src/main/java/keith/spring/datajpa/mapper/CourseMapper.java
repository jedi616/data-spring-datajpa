package keith.spring.datajpa.mapper;

import keith.domain.dto.CourseDto;
import keith.domain.jpa.entity.CourseM;
import keith.spring.datajpa.mapper.generic.GenericMapper;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface CourseMapper extends GenericMapper<CourseM, CourseDto>
{
}
