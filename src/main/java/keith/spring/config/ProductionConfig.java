package keith.spring.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import keith.domain.util.KeithUtils;

/**
 * 
 * @author kjayme
 *
 */
@Configuration
@ComponentScan(basePackages = { "keith.spring.datajpa.repository" })
@Profile("production")
public class ProductionConfig
{
    @Bean
    public DataSource dataSource(@Value("${db.username}") String username, @Value("${db.password}") String password)
    {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/my_enrollment_system");
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean
    public PropertySourcesPlaceholderConfigurer properties(ApplicationContext applicationContext, Environment environment) throws Exception
    {
        return KeithUtils.properties(applicationContext, environment);
    }
}
