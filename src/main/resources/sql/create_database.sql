-- DROP DATABASE IF EXISTS `my_enrollment_system`;
-- CREATE DATABASE  IF NOT EXISTS `my_enrollment_system` /*!40100 DEFAULT CHARACTER SET latin1 */;
-- USE `my_enrollment_system`;

-- Set this for H2Database only, Refer to: http://www.h2database.com/html/features.html#compatibility -> MySQL Compatibility Mode
SET IGNORECASE TRUE;

CREATE TABLE `department_m` (
  `department_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `department_name` varchar(100) NOT NULL,
  PRIMARY KEY (`department_id`)
);

CREATE TABLE `course_m` (
  `course_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `course_name` varchar(100) NOT NULL,
  `department_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`course_id`),
  UNIQUE KEY `course_name_UNIQUE` (`course_name`),
  KEY `department_id_idx` (`department_id`)
);

CREATE TABLE `student_m` (
  `student_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `place_of_birth` varchar(20) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `course_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`student_id`),
  KEY `course_id_idx` (`course_id`)
);

CREATE TABLE `stud_enrollment_t` (
  `std_enroll_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `semester` varchar(100) NOT NULL,
  `enrollment_date` date NOT NULL,
  `student_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`std_enroll_id`),
  KEY `student_id_idx` (`student_id`)
);

CREATE TABLE `stud_enrollment_subject_t` (
  `stud_enroll_subj_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `schedule` varchar(100) NOT NULL,
  `enrollment_date` date NOT NULL,
  `std_enroll_id` bigint(20) unsigned NOT NULL,
  `subject_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`stud_enroll_subj_id`),
  KEY `std_enroll_id_idx` (`std_enroll_id`),
  KEY `subject_id_idx` (`subject_id`)
);

CREATE TABLE `subject_m` (
  `subject_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(100) NOT NULL,
  PRIMARY KEY (`subject_id`),
  UNIQUE KEY `subject_name_UNIQUE` (`subject_name`)
);

ALTER TABLE `course_m`
  ADD CONSTRAINT `course_m_department_id_fk` 
  FOREIGN KEY (`department_id`) REFERENCES `department_m` (`department_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `student_m`
  ADD CONSTRAINT `student_m_course_id_fk` 
  FOREIGN KEY (`course_id`) REFERENCES `course_m` (`course_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `stud_enrollment_t`
  ADD CONSTRAINT `stud_enrollment_t_student_id_fk` 
  FOREIGN KEY (`student_id`) REFERENCES `student_m` (`student_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `stud_enrollment_subject_t`
  ADD CONSTRAINT `stud_enrollment_subject_t_std_enroll_id_fk`
    FOREIGN KEY (`std_enroll_id`) REFERENCES `stud_enrollment_t` (`std_enroll_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
    
ALTER TABLE `stud_enrollment_subject_t`
  ADD CONSTRAINT `stud_enrollment_subject_t_subject_id_fk`
    FOREIGN KEY (`subject_id`) REFERENCES `subject_m` (`subject_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
