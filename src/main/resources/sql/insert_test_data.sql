-- USE `mydatabase`;

-- Courses
INSERT INTO `course_m` (`course_id`, `course_name`) VALUES (1, 'BSCOMPE');
INSERT INTO `course_m` (`course_id`, `course_name`) VALUES (2, 'BSCS');
INSERT INTO `course_m` (`course_id`, `course_name`) VALUES (3, 'BSIT');
INSERT INTO `course_m` (`course_id`, `course_name`) VALUES (4, 'BSECE');

-- Students
INSERT INTO `student_m` (`name`, `gender`, `birth_date`, `course_id`) VALUES ('a1', 'Male', '2014-01-01', NULL);
INSERT INTO `student_m` (`name`, `gender`, `birth_date`, `course_id`) VALUES ('b1', 'Male', '2014-01-01', '2');
INSERT INTO `student_m` (`name`, `gender`, `birth_date`, `course_id`) VALUES ('c1', 'Male', '2014-01-01', '3');

-- Subjects
INSERT INTO `subject_m` (`subject_name`) VALUES ('subject1');
