-- Truncate table.
SET FOREIGN_KEY_CHECKS = 0;
truncate table <table_name>;
SET FOREIGN_KEY_CHECKS = 1;
select @@GLOBAL.FOREIGN_KEY_CHECKS, @@SESSION.FOREIGN_KEY_CHECKS;



-- Not used.
CREATE TABLE `hibernate_sequences` (
  `sequence_name` varchar(255) DEFAULT NULL,
  `sequence_next_hi_value` int(11) DEFAULT NULL
);

INSERT INTO `hibernate_sequences` (`sequence_name`, `sequence_next_hi_value`)
VALUES ('person_m', '1');