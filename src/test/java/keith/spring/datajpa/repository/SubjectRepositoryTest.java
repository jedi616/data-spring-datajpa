package keith.spring.datajpa.repository;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import keith.domain.jpa.entity.SubjectM;
import keith.spring.config.DataConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { DataConfig.class })
// @TransactionConfiguration(transactionManager = "transactionManager",
// defaultRollback = false)
// @Transactional//(rollbackFor = Exception.class)
@ActiveProfiles(profiles = "test")
public class SubjectRepositoryTest
{
    @Autowired
    private SubjectRepository repo;

    @Test
    @Transactional
    // (readOnly = true, rollbackFor =
    // DataIntegrityViolationException.class)//(rollbackFor = Exception.class)
    public void testRetrieveAll()
    {
        /*
         * List<SubjectM> subjects = repo.findAll(); Assert.notEmpty(subjects);
         */
        try
        {
            SubjectM subject2 = new SubjectM();
            subject2.setSubjectName("subject2");
            repo.save(subject2);

            SubjectM subject3 = new SubjectM();
            subject3.setSubjectName("subject2");
            repo.save(subject3);
        }
        catch (DataIntegrityViolationException e)
        {

        }
        catch (Exception e)
        {
            String str;
            str = "";
        }
        /*
         * List<SubjectM> subjects = repo.findAll(); for (SubjectM subject : subjects) { System.out.println(subject.getSubjectName()); }
         */
        System.out.println("ok");
        Assert.isTrue(true);
    }

    @AfterTransaction
    public void after1()
    {
        List<SubjectM> subjects = repo.findAll();
        for (SubjectM subject : subjects)
        {
            System.out.println(subject.getSubjectName());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void create()// throws Exception
    {
        try
        {
            SubjectM subject2 = new SubjectM();
            subject2.setSubjectName("subject2");
            repo.save(subject2);

            SubjectM subject3 = new SubjectM();
            subject3.setSubjectName("subject2");
            repo.save(subject3);
        }
        catch (Exception e)
        {
            String str;
            str = "";
            // throw e;
        }
    }

    // @Transactional//(rollbackFor = Exception.class)
    public void create1()// throws Exception
    {
        SubjectM subject2 = new SubjectM();
        subject2.setSubjectName("subject2");
        repo.save(subject2);

        SubjectM subject3 = new SubjectM();
        subject3.setSubjectName("subject2");
        repo.save(subject3);
    }
}
