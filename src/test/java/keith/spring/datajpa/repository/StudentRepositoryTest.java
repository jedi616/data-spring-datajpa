package keith.spring.datajpa.repository;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import keith.domain.jpa.entity.StudentM;
import keith.spring.config.DataConfig;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { DataConfig.class })
@ActiveProfiles(profiles = "test")
public class StudentRepositoryTest
{
    @Autowired
    private StudentRepository repo;

    @Test
    public void test() throws Exception
    {
        // Truncate Table student_m
        // dao.truncateTable();
        // System.out.println("student_m table truncated...");
        // System.out.println();

        // Create
        StudentM newStudent = new StudentM();
        newStudent.setName("Jimmy");
        repo.save(newStudent);
        newStudent = new StudentM();
        newStudent.setName("Ana");
        repo.save(newStudent);
        newStudent = new StudentM();
        newStudent.setName("Tom");
        repo.save(newStudent);

        // Retrieve or List
        List<StudentM> students = repo.findAll();
        displayStudents(students);

        // Update
        StudentM studentToUpdate = new StudentM();
        studentToUpdate.setStudentId(3L);
        studentToUpdate.setName("John");
        repo.save(studentToUpdate);

        // Delete
        repo.delete(2L);

        // Retrieve or List
        students = repo.findAll();
        displayStudents(students);

        // Search for Student's name
        /*
         * String hqlQuery = "Select s From StudentM s Where s.name = :nameParam"; Map<String, Object> params = new HashMap<String,
         * Object>(); params.put("nameParam", "Jimmy"); students = dao.findAll(hqlQuery, params); displayStudents(students);
         */
    }

    private void displayStudents(List<StudentM> students)
    {
        for (StudentM student : students)
        {
            System.out.print("id=" + student.getStudentId() + "; ");
            System.out.print("name=" + student.getName() + "; ");
            System.out.println("gender=" + student.getGender() + "; ");
        }
        System.out.println();
    }
}
